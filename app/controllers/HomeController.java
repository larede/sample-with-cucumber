package controllers;

import play.mvc.*;

import views.html.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import play.libs.Json;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    public Result index() {
        play.data.Form<models.Login> loginForm = play.data.Form.form(models.Login.class);
        return ok(login.render(loginForm));
    }

    @play.db.jpa.Transactional(readOnly=true)
    public Result login() {
        play.data.Form<models.Login> loginForm = play.data.Form.form(models.Login.class).bindFromRequest();
        if(loginForm.hasErrors()) {
           return badRequest(login.render(loginForm));
        } else {
            System.out.println("user: " + loginForm.get().getUser() + "; pass: " + loginForm.get().getPassword());
           if (models.Login.authenticate(loginForm.get().getUser(), loginForm.get().getPassword()) != null) {
               return ok(index.render("Your new application is ready"));
           } else {
               flash("failure", "User/password errados.");
               return badRequest(login.render(loginForm));
           }
        }
    }

    @play.db.jpa.Transactional
    public Result authenticate() {
        JsonNode input = request().body().asJson();
        String user = input.get("user").asText();
        String password = input.get("password").asText();

        ObjectNode output = Json.newObject();
        if (models.Login.authenticate(user, password) != null) {
            output.put("result", "success");
            return ok(output);
        } else {
            output.put("result", "failure");
            return notFound(output);
        }
    }
}
