package models;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Query;
import javax.persistence.Table;

import play.db.jpa.JPA;

@NamedQuery(name = "authenticate", query = "from Login where user= :user and password = :password")
@Entity
@Table(name = "login")
public class Login {
	private Long id;
	private String user = "admin";
	private String password = "admin";

	@Id
	@Column(nullable = false, insertable = true, updatable = true)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Basic
	@Column(name = "user", nullable = false, insertable = true, updatable = true, length = 20)
	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	@Basic
	@Column(name = "password", nullable = false, insertable = true, updatable = true, length = 30)
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public static Login authenticate(String user, String password) {
		Query q = JPA.em().createNamedQuery("authenticate")
				.setParameter("user", user).setParameter("password", password);
		return q.getResultList().isEmpty() ? null : (Login) q.getResultList()
				.get(0);
	}
}
