Feature: Testing Login

Scenario: Wrong login with Cucumber
  Given user go to the page ""
  And fill element input "user" with value "admin"
  And fill element input "password" with value "adminX"
  When submitting the button "Login"
  Then should appear the error message "User/password errados."

Scenario: Login with Cucumber
  Given user go to the page ""
  And fill element input "user" with value "admin"
  And fill element input "password" with value "admin"
  When submitting the button "Login"
  Then the title should be "Welcome to Play"
