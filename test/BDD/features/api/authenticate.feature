Feature: Testing Authentication

  Scenario Outline: Basic authentication
      When I send a "POST" request to "authenticate" with JSON request:
        """
        {
           "user":"<user>",
           "password":"<password>"
        }
        """
      Then the response status should be "<status>"
      And the JSON response should be:
        """
        {
           "result":"<response>"
        }
        """

    Examples: Success cases
        | user         | password         | status         | response    |
        | admin        | admin            | 200            | success     |
        | test         | test             | 200            | success     |

    Examples: Failure cases
        | user         | password         | status         | response    |
        | admin        | admiX            | 404            | failure     |
        | xpto         | xpto             | 404            | failure     |
