package BDD.steps;

import static org.junit.Assert.assertEquals;
import BDD.steps.utils.GlobalHooks;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class UISteps {
    @Given("^user go to the page \"([^\"]*)\"$")
    public void user_go_to_the_page(String page) throws Throwable {
        GlobalHooks.TEST_BROWSER.get(GlobalHooks.SERVER + ":" + GlobalHooks.PORT + "/" + page);
    }

    @Given("^fill element input \"([^\"]*)\" with value \"([^\"]*)\"$")
    public void fill_element_input_with_value(String element, String value) throws Throwable {
        GlobalHooks.TEST_BROWSER.findElementByXPath("//input[@name='" + element + "']").sendKeys(value);
    }

    @When("^submitting the button \"([^\"]*)\"$")
    public void submitting_the_button(String button) throws Throwable {
        GlobalHooks.TEST_BROWSER.findElementByXPath("//button[text()='" + button + "']").submit();
    }

	@Then("^the title should be \"([^\"]*)\"$")
	public void the_title_should_be(String title) throws Throwable {
        assertEquals(GlobalHooks.TEST_BROWSER.getTitle(), title);
	}

    @Then("^should appear the error message \"([^\"]*)\"$")
    public void should_appear_the_error_message(String message) throws Throwable {
        assertEquals(GlobalHooks.TEST_BROWSER.findElementByClassName("text-error").getText(), message);
    }
}
