package BDD.steps;

import static org.junit.Assert.assertEquals;
import static play.test.Helpers.contentAsString;
import static play.test.Helpers.route;
import play.libs.Json;
import play.mvc.Http.RequestBuilder;
import play.mvc.Result;
import BDD.steps.utils.GlobalHooks;

import com.fasterxml.jackson.databind.JsonNode;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class APISteps {
	Result result;

	@When("^I send a \"([^\"]*)\" request to \"([^\"]*)\" with JSON request:$")
	public void i_send_a_request_to_with_JSON_request(String op, String url,
			String jsonRequest) throws Throwable {
		result = null;
		JsonNode json = Json.parse(jsonRequest);

		RequestBuilder request = new RequestBuilder();
		request.bodyJson(json).method(op)
				.uri(GlobalHooks.SERVER + ":" + GlobalHooks.PORT + "/" + url);
		result = route(request);
	}

	@Then("^the response status should be \"(\\d+)\"$")
	public void the_response_status_should_be(int httpStatus) throws Throwable {
		assertEquals(httpStatus, result.status());
	}

	@Then("^the JSON response should be:$")
	public void the_JSON_response_should_be(String jsonResponse)
			throws Throwable {
		assertEquals(Json.parse(jsonResponse),
				Json.parse(contentAsString(result)));
	}
}
