package BDD.steps.utils;

import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import static play.test.Helpers.start;
import static play.test.Helpers.testBrowser;
import static play.test.Helpers.testServer;
import play.Mode;
import play.Application;
import play.inject.guice.GuiceApplicationBuilder;
import play.test.TestBrowser;
import play.test.TestServer;
import cucumber.api.java.Before;

public class GlobalHooks {
	public static String SERVER = "http://localhost";
	public static int PORT = 3333;
	private static TestServer TEST_SERVER;
	private static boolean initialised = false;
    public static HtmlUnitDriver TEST_BROWSER;

	@Before
	public void before() {
		if (!initialised) {
			Application application = new GuiceApplicationBuilder().in(
					Mode.TEST).build();

			TEST_SERVER = testServer(PORT, application);
            TEST_BROWSER = new HtmlUnitDriver();
            start(TEST_SERVER);
			initialised = true;
			Runtime.getRuntime().addShutdownHook(new Thread() {
				@Override
				public void run() {
					TEST_BROWSER.quit();
					TEST_SERVER.stop();
				}
			});
		}
	}
}
