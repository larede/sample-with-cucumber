name := """sample-with-cucumber"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaWs,
  javaJpa,
  "info.cukes" % "cucumber-java" % "1.2.4" % "test",
  "info.cukes" % "cucumber-junit" % "1.2.4" % "test",
  "junit" % "junit" % "4.12" % "test",
  "org.hibernate" % "hibernate-entitymanager" % "4.3.5.Final",
  "org.postgresql" % "postgresql" % "9.3-1100-jdbc41",
  "org.codehaus.jackson" % "jackson-core-asl" % "1.1.0"
)

unmanagedResourceDirectories in Test <+= baseDirectory( _ / "test/features" )

javaOptions in Test += "-Dconfig.file=conf/application.test.conf"
